import { useState, useLayoutEffect } from 'react';

const queries = [
  '(max-width: 767px)',
  '(min-width: 768px)',
  '(min-width: 1200px)',
];

export const useMatchMedia = () => {
  const mediaQueryLists = queries.map((query) => matchMedia(query));

  const getMatches = () => mediaQueryLists.map((list) => list.matches);

  const [matches, setMatches] = useState(getMatches);

  useLayoutEffect(() => {
    const handler = () => setMatches(getMatches);

    mediaQueryLists.forEach((list) => list.addEventListener('change', handler));

    return () =>
      mediaQueryLists.forEach((list) =>
        list.removeEventListener('change', handler)
      );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return ['isMobile', 'isTablet', 'isDesktop'].reduce(
    (acc, screen, index) => ({
      ...acc,
      [screen]: matches[index],
    }),
    {}
  );
};
