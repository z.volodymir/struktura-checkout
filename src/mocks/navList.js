import { ReactComponent as FacebookIcon } from '@assets/icons/socials/facebook.svg';
import { ReactComponent as InstagramIcon } from '@assets/icons/socials/instagram.svg';
import { ReactComponent as YoutubeIcon } from '@assets/icons/socials/youtube.svg';

export const navList = [
  {
    title: 'about us',
    links: ['Demo', 'Blog', 'Features', 'FAQ', 'Contact us'],
  },
  {
    title: 'service legal terms',
    links: ['Eula', 'refund policy', 'cookie notice', 'privacy notice'],
  },
  {
    title: 'Partners',
    links: ['affiliate program', 'reseller program'],
  },
  {
    title: 'Follow us',
    links: [<FacebookIcon />, <InstagramIcon />, <YoutubeIcon />],
  },
];
