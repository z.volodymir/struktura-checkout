import { ReactComponent as KikIcon } from '@assets/icons/app/Kik.svg';
import { ReactComponent as FbIcon } from '@assets/icons/app/fb.svg';
import { ReactComponent as HangoutsIcon } from '@assets/icons/app/hangouts.svg';
import { ReactComponent as InstIcon } from '@assets/icons/app/inst.svg';
import { ReactComponent as LineIcon } from '@assets/icons/app/line.svg';
import { ReactComponent as MessIcon } from '@assets/icons/app/mess.svg';
import { ReactComponent as RedditIcon } from '@assets/icons/app/reddit.svg';
import { ReactComponent as SignalIcon } from '@assets/icons/app/signal.svg';
import { ReactComponent as SkypeIcon } from '@assets/icons/app/skype.svg';
import { ReactComponent as SnapchatIcon } from '@assets/icons/app/snapchat.svg';
import { ReactComponent as TgIcon } from '@assets/icons/app/tg.svg';
import { ReactComponent as TiktokIcon } from '@assets/icons/app/tiktok.svg';
import { ReactComponent as TinderIcon } from '@assets/icons/app/tinder.svg';
import { ReactComponent as ViberIcon } from '@assets/icons/app/viber.svg';
import { ReactComponent as WechatIcon } from '@assets/icons/app/wechat.svg';
import { ReactComponent as WhatsappIcon } from '@assets/icons/app/whatsapp.svg';
import { ReactComponent as YoutubeIcon } from '@assets/icons/app/youtube.svg';
import { ReactComponent as ZoomIcon } from '@assets/icons/app/zoom.svg';

export const appList = [
  <KikIcon />,
  <FbIcon />,
  <HangoutsIcon />,
  <InstIcon />,
  <LineIcon />,
  <MessIcon />,
  <RedditIcon />,
  <SignalIcon />,
  <SkypeIcon />,
  <SnapchatIcon />,
  <TgIcon />,
  <TiktokIcon />,
  <TinderIcon />,
  <ViberIcon />,
  <WechatIcon />,
  <WhatsappIcon />,
  <YoutubeIcon />,
  <ZoomIcon />,
];
