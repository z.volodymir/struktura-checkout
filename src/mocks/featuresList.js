import { ReactComponent as SocialIcon } from '@assets/icons/features/social.svg';
import { ReactComponent as CallsIcon } from '@assets/icons/features/calls.svg';
import { ReactComponent as ContactIcon } from '@assets/icons/features/contact.svg';
import { ReactComponent as SmsIcon } from '@assets/icons/features/sms.svg';
import { ReactComponent as LocationIcon } from '@assets/icons/features/location.svg';
import { ReactComponent as GalleryIcon } from '@assets/icons/features/gallery.svg';
import { ReactComponent as KeyboardIcon } from '@assets/icons/features/keyboard.svg';
import { ReactComponent as BrowserIcon } from '@assets/icons/features/browser.svg';
import { ReactComponent as HeartIcon } from '@assets/icons/features/heart.svg';
import { ReactComponent as EmailIcon } from '@assets/icons/features/email.svg';
import { ReactComponent as WebIcon } from '@assets/icons/features/web.svg';

export const featuresList = [
  {
    icon: <SocialIcon />,
    text: 'Feature 1',
  },
  {
    icon: <CallsIcon />,
    text: 'Feature 2',
  },
  {
    icon: <ContactIcon />,
    text: 'Feature 3',
  },
  {
    icon: <SmsIcon />,
    text: 'Feature 4',
  },
  {
    icon: <LocationIcon />,
    text: 'Feature 5',
  },
  {
    icon: <GalleryIcon />,
    text: 'Feature 6',
  },
  {
    icon: <KeyboardIcon />,
    text: 'Feature 7',
  },
  {
    icon: <BrowserIcon />,
    text: 'Feature 8',
  },
  {
    icon: <HeartIcon />,
    text: 'Feature 9',
  },
  {
    icon: <EmailIcon />,
    text: 'Feature 10',
  },
  {
    icon: <WebIcon />,
    text: 'Feature 11',
  },
];
