export const postData = async (data) => {
  try {
    const response = await fetch('http://localhost:3001/orders', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });

    return await response.json();
  } catch (error) {
    throw new Error(error.message);
  }
};

export const getCoupon = async () => {
  try {
    const response = await fetch('http://localhost:3001/coupons');

    return await response.json();
  } catch (error) {
    throw new Error(error.message);
  }
};
