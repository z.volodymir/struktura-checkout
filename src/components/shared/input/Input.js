import { forwardRef } from 'react';
import classNames from 'classnames';

import styles from './Input.module.scss';

const Input = forwardRef(({ variant, isError, ...props }, ref) => {
  return (
    <input
      {...props}
      ref={ref}
      className={classNames(styles.input, {
        [styles.email]: variant === 'email',
        [styles.cardName]: variant === 'cardName',
        [styles.cardNumber]: variant === 'cardNumber',
        [styles.cardDate]: variant === 'cardDate',
        [styles.cardCode]: variant === 'cardCode',
        [styles.coupon]: variant === 'coupon',
        [styles.error]: isError,
      })}
    />
  );
});

export default Input;
