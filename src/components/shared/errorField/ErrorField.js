import classNames from 'classnames';

import styles from './ErrorField.module.scss';

const ErrorField = ({ children, position, isSuccess = false }) => {
  return (
    <p
      className={classNames(styles.error, {
        [styles.top]: position === 'top',
        [styles.bottom]: position === 'bottom',
        [styles.success]: isSuccess,
      })}
    >
      {children}
    </p>
  );
};

export default ErrorField;
