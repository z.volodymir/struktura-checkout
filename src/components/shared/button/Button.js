import classNames from 'classnames';

import styles from './Button.module.scss';

const Button = ({ children, variant, onClick = () => {}, ...otherProps }) => {
  return (
    <button
      className={classNames(styles.button, {
        [styles.submit]: variant === 'submit',
        [styles.apple]: variant === 'apple',
        [styles.pay]: variant === 'pay',
        [styles.icon]: variant === 'icon',
        [styles.close]: variant === 'close',
        [styles.check]: variant === 'check',
      })}
      onClick={onClick}
      {...otherProps}
    >
      {children}
    </button>
  );
};

export default Button;
