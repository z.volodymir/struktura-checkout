import styles from './Link.module.scss';

const Link = ({ children, onClick = () => {}, ...otherProps }) => {
  return (
    <a className={styles.link} onClick={onClick} {...otherProps}>
      {children}
    </a>
  );
};

export default Link;
