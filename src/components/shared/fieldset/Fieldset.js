import styles from './Fieldset.module.scss';

const Fieldset = ({ children, legend }) => {
  return (
    <fieldset className={styles.fieldset}>
      <legend className={styles.legend}>{legend}</legend>
      {children}
    </fieldset>
  );
};

export default Fieldset;
