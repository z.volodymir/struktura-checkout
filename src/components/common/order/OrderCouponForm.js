import { useState } from 'react';
import { useForm } from 'react-hook-form';
import Popup from 'reactjs-popup';
import { ReactComponent as CloseIcon } from '@assets/icons/close.svg';
import { ReactComponent as ApplyIcon } from '@assets/icons/apply.svg';
import { getCoupon } from '@api';
import Input from '@components/shared/input/Input';
import Button from '@components/shared/button/Button';
import ErrorField from '@components/shared/errorField/ErrorField';

import styles from './OrderCouponForm.module.scss';

const OrderCouponForm = ({ setValue, isOpenedPopup, setIsOpenedPopup }) => {
  const [isShowError, setIsShowError] = useState(false);
  const [hasCoupon, setHasCoupon] = useState('');

  const { register, handleSubmit } = useForm();

  const onSubmit = (data) => {
    const couponValue = data.coupon;

    if (process.env.NODE_ENV === 'development') {
      getCoupon()
        .then((response) => {
          const isIncludeCoupon = response.includes(couponValue);

          if (isIncludeCoupon) {
            setHasCoupon(true);
            setValue('coupon', couponValue);
          } else {
            setHasCoupon(false);
          }

          setIsShowError(true);
        })
        .catch((error) => console.log(error.message));
    } else {
      if (couponValue === 'promo10') {
        setHasCoupon(true);
        setValue('coupon', couponValue);
      } else {
        setHasCoupon(false);
      }

      setIsShowError(true);
    }
  };

  const closeModal = () => setIsOpenedPopup(false);

  return (
    <Popup
      open={isOpenedPopup}
      closeOnDocumentClick
      lockScroll
      onClose={closeModal}
    >
      <div className={styles.modal}>
        <span className={styles.button}>
          <Button variant="close" onClick={closeModal}>
            <CloseIcon className={styles.closeIcon} />
          </Button>
        </span>
        <form
          className={styles.form}
          onSubmit={handleSubmit(onSubmit)}
          id="coupon"
        >
          <Input
            type="text"
            variant="coupon"
            placeholder="Enter your promo code"
            {...register('coupon')}
          />
          <Button type="submit" form="coupon" variant="check">
            <ApplyIcon className={styles.applyIcon} />
          </Button>
        </form>
      </div>
      {isShowError &&
        (hasCoupon ? (
          <ErrorField position="bottom" isSuccess>
            Your coupon has been applied
          </ErrorField>
        ) : (
          <ErrorField position="bottom">Invalid coupon code :(</ErrorField>
        ))}
    </Popup>
  );
};

export default OrderCouponForm;
