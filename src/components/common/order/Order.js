import OrderStatus from './OrderStatus';
import OrderForm from './OrderForm';

import styles from './Order.module.scss';

const Order = () => {
  return (
    <div className={styles.wrapper}>
      <OrderStatus />
      <OrderForm />
    </div>
  );
};

export default Order;
