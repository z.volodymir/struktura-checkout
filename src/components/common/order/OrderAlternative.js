import { useState } from 'react';
import { ReactComponent as PaypalIcon } from '@assets/icons/cards/paypal.svg';
import { ReactComponent as BtcIcon } from '@assets/icons/cards/btc.svg';
import { ReactComponent as AngleDownIcon } from '@assets/icons/angle-down.svg';
import { ReactComponent as AngleUpIcon } from '@assets/icons/angle-up.svg';
import Button from '@components/shared/button/Button';
import Fieldset from '@components/shared/fieldset/Fieldset';

import styles from './OrderAlternative.module.scss';
import classNames from 'classnames';

const OrderAlternative = () => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <Fieldset legend="Or alternative methods">
      <div
        className={classNames(styles.alternative, {
          [styles.disable]: isOpen,
        })}
      >
        <span className={styles.icons}>
          <PaypalIcon />
          <BtcIcon />
        </span>
        <Button type="button" variant="icon" onClick={() => setIsOpen(!isOpen)}>
          {isOpen ? (
            <AngleUpIcon className={styles.angleIcon} />
          ) : (
            <AngleDownIcon className={styles.angleIcon} />
          )}
        </Button>
      </div>

      {isOpen && (
        <div className={styles.buttonGroup}>
          <Button variant="pay">
            <PaypalIcon />
          </Button>
          <Button variant="pay">
            <BtcIcon />
          </Button>
        </div>
      )}
    </Fieldset>
  );
};

export default OrderAlternative;
