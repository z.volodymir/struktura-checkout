import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { ReactComponent as AppleIcon } from '@assets/icons/apple.svg';
import { postData } from '@api';
import Button from '@components/shared/button/Button';
import Input from '@components/shared/input/Input';
import Fieldset from '@components/shared/fieldset/Fieldset';
import ErrorField from '@components/shared/errorField/ErrorField';
import Link from '@components/shared/link/Link';
import OrderAlternative from './OrderAlternative';
import OrderCouponForm from './OrderCouponForm';

import styles from './OrderForm.module.scss';
import classNames from 'classnames';

const OrderForm = () => {
  const [isOpenedPopup, setIsOpenedPopup] = useState(false);

  const {
    register,
    handleSubmit,
    reset,
    setValue,
    formState: { errors, isSubmitSuccessful, isValid },
  } = useForm({ mode: 'all' });

  const onSubmit = (data) => {
    if (process.env.NODE_ENV === 'development') {
      postData(data)
        .then((response) => console.log(response))
        .catch((error) => console.log(error.message));
    } else {
      console.log(data);
    }
  };

  useEffect(() => {
    reset();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isSubmitSuccessful]);

  return (
    <>
      <form
        className={styles.form}
        onSubmit={handleSubmit(onSubmit)}
        id="order"
      >
        <Button variant="apple">
          <AppleIcon />
        </Button>

        <Fieldset legend="Or pay with card">
          <label
            className={classNames(styles.label, {
              [styles.required]: errors.email?.type === 'required',
            })}
            htmlFor="email"
          >
            Email
          </label>
          {errors.email?.type === 'required' && (
            <ErrorField position="top">Required</ErrorField>
          )}
          <Input
            type="email"
            variant="email"
            placeholder="examplemail@mail.com"
            isError={errors.email ? true : false}
            {...register('email', {
              required: true,
              pattern: {
                value: /^(.+)@(.+)\.(.+)$/,
                message: 'Your email is incomplete',
              },
            })}
          />
          {errors?.email?.message && (
            <ErrorField position="bottom">{errors.email.message}</ErrorField>
          )}

          <label
            className={classNames(styles.label, {
              [styles.required]: errors.cardNumber?.type === 'required',
            })}
            htmlFor="cardNumber"
          >
            Card information
          </label>
          {(errors.cardNumber?.type === 'required' ||
            errors.cardDate?.type === 'required' ||
            errors.cardCode?.type === 'required') && (
            <ErrorField position="top">Required</ErrorField>
          )}
          <div className={styles.group}>
            <Input
              type="number"
              variant="cardNumber"
              placeholder="0000 0000 0000 0000"
              isError={errors.cardNumber ? true : false}
              {...register('cardNumber', {
                required: true,
                pattern: {
                  value: /^\d{16}$/,
                  message: 'Your card number is invalid',
                },
              })}
            />
            <Input
              type="text"
              variant="cardDate"
              placeholder="MM/YYYY"
              isError={errors.cardDate ? true : false}
              {...register('cardDate', {
                required: true,
                pattern: {
                  value: /^(\d{2})\/(\d{4})$/,
                  message: "Your card's expiration date is incomplete",
                },
              })}
            />
            <Input
              type="number"
              variant="cardCode"
              placeholder="CVV"
              isError={errors.cardCode ? true : false}
              {...register('cardCode', {
                required: true,
                pattern: /^\d{3,4}$/,
              })}
            />
          </div>
          {errors?.cardNumber?.message && (
            <ErrorField position="bottom">
              {errors.cardNumber.message}
            </ErrorField>
          )}
          {errors?.cardDate?.message && (
            <ErrorField position="bottom">{errors.cardDate.message}</ErrorField>
          )}

          <label
            className={classNames(styles.label, {
              [styles.required]: errors.cardName?.type === 'required',
            })}
            htmlFor="cardName"
          >
            Name on card
          </label>
          {errors.cardName?.type === 'required' && (
            <ErrorField position="top">Required</ErrorField>
          )}
          <Input
            type="text"
            variant="cardName"
            placeholder="Enter cardholder name"
            isError={errors.cardName ? true : false}
            {...register('cardName', {
              required: true,
              pattern: /^[A-Za-z]+/i,
            })}
          />
          {errors?.cardName?.message && (
            <ErrorField position="bottom">{errors.cardName.message}</ErrorField>
          )}
        </Fieldset>

        <Link onClick={() => setIsOpenedPopup(true)}>I have a coupon code</Link>

        <Button type="submit" variant="submit" form="order" disabled={!isValid}>
          Submit order
        </Button>

        <OrderAlternative />
      </form>

      <OrderCouponForm
        setValue={setValue}
        isOpenedPopup={isOpenedPopup}
        setIsOpenedPopup={setIsOpenedPopup}
      />
    </>
  );
};

export default OrderForm;
