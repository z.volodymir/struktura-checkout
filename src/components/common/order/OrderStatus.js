import styles from './OrderStatus.module.scss';

const OrderStatus = () => {
  return (
    <div className={styles.wrapper}>
      <h1 className={styles.title}>Finish your order</h1>
      <ul className={styles.list}>
        <li className={styles.item}>
          <p className={styles.prop}>Plan</p>
          <p className={styles.size}>Full Pack</p>
        </li>
        <li className={styles.item}>
          <p className={styles.prop}>Subscription Period</p>
          <p className={styles.month}>3 months</p>
        </li>
        <li className={styles.item}>
          <p className={styles.prop}>1 month price</p>
          <p className={styles.price}>$ 29.99</p>
        </li>
      </ul>
    </div>
  );
};

export default OrderStatus;
