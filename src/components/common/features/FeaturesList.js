import { featuresList } from '@mocks/featuresList';
import FeatureAppList from './FeatureAppList';

import styles from './FeaturesList.module.scss';

const FeaturesList = () => {
  return (
    <ul className={styles.list}>
      {featuresList.map(({ icon, text }, index) => {
        return (
          <li key={text} className={styles.item}>
            <p className={styles.text}>
              {icon} {text}
            </p>
            {index === 0 && <FeatureAppList />}
          </li>
        );
      })}
    </ul>
  );
};

export default FeaturesList;
