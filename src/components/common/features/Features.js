import { ReactComponent as FeatureIcon } from '@assets/icons/features.svg';
import { useMatchMedia } from '@hooks/useMatchMedia';
import FeaturesList from './FeaturesList';

import styles from './Features.module.scss';

const Features = () => {
  const { isMobile } = useMatchMedia();

  return (
    <>
      {!isMobile && <FeatureIcon />}

      <p className={styles.text}>You get all these features:</p>

      <FeaturesList />
    </>
  );
};

export default Features;
