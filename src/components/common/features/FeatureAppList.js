import { appList } from '@mocks/appList';

import styles from './FeatureAppList.module.scss';

const FeatureAppList = () => {
  return (
    <ul className={styles.list}>
      {appList.map((app, index) => {
        return <li key={index}>{app}</li>;
      })}
    </ul>
  );
};

export default FeatureAppList;
