import { ReactComponent as CheckIcon } from '@assets/icons/check.svg';
import { ReactComponent as StepIcon } from '@assets/icons/step.svg';

import styles from './Steps.module.scss';

const Steps = () => {
  return (
    <div className={styles.wrapper}>
      <span className={styles.icon}>
        <CheckIcon />
      </span>
      <span className={styles.icon}>
        <CheckIcon />
      </span>
      <span className={styles.icon}>
        <CheckIcon />
      </span>
      <span className={styles.icon}>
        <StepIcon />
      </span>
    </div>
  );
};

export default Steps;
