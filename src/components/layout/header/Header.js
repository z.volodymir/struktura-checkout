/* eslint-disable jsx-a11y/anchor-is-valid */
import { useMatchMedia } from '@hooks/useMatchMedia';
import { ReactComponent as BurgerIcon } from '@assets/icons/burger.svg';
import Container from '@components/shared/container/Container';
import Logo from '@components/shared/logo/Logo';

import styles from './Header.module.scss';

const Header = () => {
  const { isMobile } = useMatchMedia();

  return (
    <header className={styles.header}>
      <Container>
        <div className={styles.content}>
          <a href="#">
            <Logo />
          </a>
          {isMobile && (
            <button className={styles.menu}>
              <BurgerIcon />
            </button>
          )}
        </div>
      </Container>
    </header>
  );
};

export default Header;
