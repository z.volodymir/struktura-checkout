import Container from '@components/shared/container/Container';
import FooterNavigation from './FooterNavigation';
import FooterInfo from './FooterInfo';
import FooterAbout from './FooterAbout';

import styles from './Footer.module.scss';

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <Container>
        <div className={styles.content}>
          <FooterAbout />
          <FooterNavigation />
        </div>

        <hr className={styles.separator} />

        <FooterInfo />
      </Container>
    </footer>
  );
};

export default Footer;
