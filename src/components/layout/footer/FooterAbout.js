import Logo from '@components/shared/logo/Logo';

import styles from './FooterAbout.module.scss';

const FooterAbout = () => {
  return (
    <div className={styles.about}>
      <Logo />
      <p className={styles.description}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. A massa
        scelerisque venenatis orci urna sed lobortis. Vitae cras augue orci cum
        scelerisque maecenas risus arcu. Gravida suscipit amet nullam augue vel,
        non amet pharetra elementum. Leo suspendisse amet condimentum eu gravida
        eu nisl. Iaculis ac turpis nibh mauris mollis cras faucibus gravida sit.
      </p>
    </div>
  );
};

export default FooterAbout;
