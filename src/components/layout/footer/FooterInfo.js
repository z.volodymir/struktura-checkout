/* eslint-disable jsx-a11y/anchor-is-valid */
import { ReactComponent as AngleDownIcon } from '@assets/icons/angle-down.svg';
import { ReactComponent as Secure1Icon } from '@assets/icons/secure/1.svg';
import { ReactComponent as Secure2Icon } from '@assets/icons/secure/2.svg';
import { ReactComponent as Secure3Icon } from '@assets/icons/secure/3.svg';

import styles from './FooterInfo.module.scss';

const FooterInfo = () => {
  return (
    <div className={styles.info}>
      <p className={styles.lang}>
        english <AngleDownIcon />
      </p>

      <a href="#" className={styles.terms}>
        Terms of service
      </a>

      <div className={styles.secure}>
        <p className={styles.text}>Secure internet payments</p>
        <div className={styles.icons}>
          <Secure1Icon />
          <Secure2Icon />
          <Secure3Icon />
        </div>
      </div>

      <p className={styles.rights}>© 2022 LOGO | All Rights Reserved.</p>
    </div>
  );
};

export default FooterInfo;
