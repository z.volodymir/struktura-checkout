/* eslint-disable jsx-a11y/anchor-is-valid */
import { useState } from 'react';
import classNames from 'classnames';
import { navList } from '@mocks/navList';
import { ReactComponent as AngleDownIcon } from '@assets/icons/angle-down.svg';
import { ReactComponent as AngleUpIcon } from '@assets/icons/angle-up.svg';

import styles from './FooterNavigation.module.scss';

const FooterNavigation = () => {
  const [selected, setSelected] = useState(null);

  const selectList = (list) => {
    if (selected === list) {
      setSelected(null);
    } else {
      setSelected(list);
    }
  };

  return (
    <nav className={styles.navigation}>
      <ul className={styles.navList}>
        {navList &&
          navList.map(({ title, links }, index) => {
            const lastItem = navList.length === index + 1;

            return (
              <li key={title} className={styles.navItem}>
                <h3
                  className={styles.navTitle}
                  onClick={() => !lastItem && selectList(title)}
                >
                  {title}
                  {!lastItem && (
                    <span className={styles.angle}>
                      {selected === title ? <AngleUpIcon /> : <AngleDownIcon />}
                    </span>
                  )}
                </h3>
                <ul
                  className={classNames(styles.navSubList, {
                    [styles.show]: selected === title,
                  })}
                >
                  {links.map((link, index) => (
                    <li key={index}>
                      <a href="#" className={styles.navLink}>
                        {link}
                      </a>
                    </li>
                  ))}
                </ul>
              </li>
            );
          })}
      </ul>
    </nav>
  );
};

export default FooterNavigation;
