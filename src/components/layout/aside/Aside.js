import classNames from 'classnames';

import styles from './Aside.module.scss';

const Aside = ({ children, text }) => {
  return (
    <aside
      className={classNames(styles.wrapper, {
        [styles.text]: text,
      })}
    >
      {children}
    </aside>
  );
};

export default Aside;
