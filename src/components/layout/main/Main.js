import Container from '@components/shared/container/Container';
import Steps from '@components/common/steps/Steps';
import Features from '@components/common/features/Features';
import Order from '@components/common/order/Order';
import Aside from '@components/layout/aside/Aside';

import styles from './Main.module.scss';

const Main = () => {
  return (
    <main className={styles.main}>
      <Container>
        <div className={styles.content}>
          <Steps />
          <section className={styles.section}>
            <Aside>
              <Features />
            </Aside>
            <Order />
            <Aside text>
              <p className={styles.text}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit ut
                aliquam, purus sit amet luctus venenatis, lectus magna Lorem
                ipsum dolor sit amet.
              </p>
            </Aside>
          </section>
        </div>
      </Container>
    </main>
  );
};

export default Main;
